<?php

/**
 * Obtain the home_timeline RSS feed using OAuth
 *
 * Although this example uses your user token/secret, you can use
 * the user token/secret of any user who has authorised your application.
 *
 * Instructions:
 * 1) If you don't have one already, create a Twitter application on
 *      https://dev.twitter.com/apps
 * 2) From the application details page copy the consumer key and consumer
 *      secret into the place in this code marked with (YOUR_CONSUMER_KEY
 *      and YOUR_CONSUMER_SECRET)
 * 3) From the application details page copy the access token and access token
 *      secret into the place in this code marked with (A_USER_TOKEN
 *      and A_USER_SECRET)
 * 4) Visit this page using your web browser.
 *
 * @author themattharris
 */

require 'tmhOAuth-master/tmhOAuth.php';
require 'tmhOAuth-master/tmhUtilities.php';
$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => 'wF2juJGUGOnFMvwgqyzog',
  'consumer_secret' => 'sQRsJcjToaRsNnjUR4FdIkYpyZazZlxmnCrjTDWg',
  'user_token'      => '164594563-xSNnXZFwVgrlXdm96hCa65A2PrpOqHyERpvEHgSf',
  'user_secret'     => 'FxHSRVrjUXTeINN6gAkJBWpSeWnepiferyBrFft1kk8',
));

$code = $tmhOAuth->request('GET', $tmhOAuth->url('1/statuses/user_timeline', 'rss'), array('screen_name' => 'suthichai'));

if ($code == 200) {
  header('Content-Type: application/rss+xml; charset=utf-8');
  echo $tmhOAuth->response['response'];
} else {
  tmhUtilities::pr(htmlentities($tmhOAuth->response['response']));
}